<?php
if ($_SERVER['REQUEST_METHOD'] == "POST") {
    if(isset($_POST['useDB'])){
        $conn = new mysqli("localhost", "root", "", $_POST['useDB']);
    }else{
        $conn = new mysqli("localhost", "root", "");
    }
    $res = $conn->query($_POST['query']);
    if(!$res){
        echo "Query: ".$_POST['query']."<br>";
        echo "Error code: ".$conn->errno."<br>";
        echo $conn->error;
        exit();
    }
    if(gettype($res) == "boolean" && $res){
        echo "sucsses";
    }else{
        echo "<table>";
        while ($row = $res->fetch_assoc()) {
            echo "<tr>";
            foreach ($row as $key => $val) {
                echo "<td>{$row[$key]}</td>";
            }
            echo "</tr>";
        }
        echo "</table>";
    }
} else {
    header("location: ../index.php");
}