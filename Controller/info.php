<?php
require_once '../Librari/INFOCLASS.php';
require_once '../Librari/string.php';
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $conn1 = new mysqli("localhost", "root", "", "information_schema");
    $info = new INFO($conn1, $tableQuery);
    if (isset($_POST['useDB'])) {
        $conn2 = new mysqli("localhost", "root", "", "mysql");
        $tableQuery = TABLE_QUERY . " '{$_POST['useDB']}'";
        $viewQuery = VIEW_QUERY . " '{$_POST['useDB']}'";
        $procedQuery = PROCEDURE_QUERY . " '{$_POST['useDB']}'";
        $funcQuery = FUNCTION_QUERY . " '{$_POST['useDB']}'";
        $triggerQuery = TRIGGER_QUERY . " '{$_POST['useDB']}'";
        $eventQuery = EVENT_QUERY . " '{$_POST['useDB']}'";
        // table info
        $num = $info->getRows();
        echo "<p>Tables({$num})</p>";
        $info->infoDatabase(TABLE);
        // view info
        $info->setQuery($viewQuery);
        $num = $info->getRows();
        echo "<p>Views({$num})</p>";
        $info->infoDatabase(VIEW);
        //procedure info
        $info->setConn($conn2);
        $info->setQuery($procedQuery);
        $num = $info->getRows();
        echo "<p>Procedurs({$num})</p>";
        $info->infoDatabase(PROCEDURE);
        //function info
        $info->setQuery($funcQuery);
        $num = $info->getRows();
        echo "<p>Functions({$num})</p>";
        $info->infoDatabase(FUNCTIONS);
        //trigger info
        $info->setConn($conn1);
        $info->setQuery($triggerQuery);
        $num = $info->getRows();
        echo "<p>Triggers({$num})</p>";
        $info->infoDatabase(TRIGGER);
        //event info
        $info->setQuery($eventQuery);
        $num = $info->getRows();
        echo "<p>Events({$num})</p>";
        $info->infoDatabase(EVENT);
    }
    if (isset($_POST['tb_name']) && isset($_POST['to_db'])) {
        $fieldQuery = FIELD_QUERY . $_POST['to_db'] . ADD . $_POST['tb_name'];
        $indexQuery=
         $conn1->query($fieldQuery);
        // FIELD info
        $num = $info->getRows();
        echo "<p>Tables({$num})</p>";
        $info->infoDatabase(TABLE_FIELD);
    }
}
