<?php
if($_SERVER['REQUEST_METHOD']=="POST"){
    require_once("../Librari/Show.php");
    $res=explode("-",$_POST['act']);
    $tabtagst="<li class='xdebug tables'>";
    $tagstart="<li class='xdebug'>";
    $tagend="</li>";
    switch($res[1]){
        case "ta":
            $tabconn=new mysqli("localhost","root","");
            $showTA = new SHOW($tabconn,$res[0],$tabtagst,$tagend);
            $showTA->showTables();
            break;
        case "vi":
            $viconn=new mysqli("localhost","root","","information_schema");
            $showVi = new SHOW($viconn,$res[0],$tagstart,$tagend);
            $showVi->showViews();
            break;
        case "pr":
            $prconn = new mysqli("localhost","root","","mysql");
            $showPr = new SHOW($prconn,$res[0],$tagstart,$tagend);
            $showPr->showProc();
            break;
        case "fun":
            $fuconn = new mysqli("localhost","root","","mysql");
            $showFu = new SHOW($fuconn,$res[0],$tagstart,$tagend);
            $showFu->showFunc();
            break;
        case "tr":
            $trconn=new mysqli("localhost","root","");
            $showTr = new SHOW($trconn,$res[0],$tagstart,$tagend);
            $showTr->showTriggers();
            break;
        case "ev":
            $evconn=new mysqli("localhost","root","","information_schema");
            $showEv = new SHOW($evconn,$res[0],$tagstart,$tagend);
            $showEv->showEvent();
            break;
    }
}