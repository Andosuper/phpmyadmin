<!DOCTYPE html>
<html>
<head>
    <title>phpMyAdmin</title>
    <link rel="stylesheet" href="View/CSS/style.css" type="text/css">
    <link rel="stylesheet" href="View/CSS/bootstrap.min.css" type="text/css">
    <script src="View/JS/jquery.js"></script>
    <script src="View/JS/script.js"></script>
</head>
<body>
<div class="manu">
    <div class="col-lg-12 text-center">
        <ul class="list-inline">
            <li>Database:</li>
            <li class="dbname"></li>
            <li class="fortb"></li>
            <li class="tbname"></li>
        </ul>
    </div>
</div>
<div class="container-fluid">
    <div class="col-lg-3 col-md-3">
        <div class="db_resurs">
            <ul class="db"></ul>
        </div>
    </div>
    <div class="col-lg-9 col-md-9">
        <div class="creatTb">
            <span class="close">x</span>
            <fieldset id="myform">
                <label for="tbname">Name of table:</label>
                <input class="val" type="text" id="tbname" name="tbname">
                <label for="tbtype">type of table:</label>
                <select class="val" id="tbtype" name="tbtype">
                    <option value="InnoDB">InnoDB</option>
                    <option value="MyISAM">MyISAM</option>
                    <option value="IBMDB2I">IBMDB2I</option>
                    <option value="MERGE">MERGE</option>
                    <option value="MEMORY">MEMORY</option>
                    <option value="EXAMPLE">EXAMPLE</option>
                    <option value="FEDERATED">FEDERATED</option>
                    <option value="ARCHIVE">ARCHIVE</option>
                    <option value="CSV">CSV</option>
                    <option value="BLACKHOLE">BLACKHOLE</option>
                </select>
                <button id="ADD">Add column</button>
                <table id="creattbl">
                    <tr>
                        <th>Column name</th>
                        <th>Data type</th>
                        <th>Default</th>
                        <th>PK</th>
                        <th>Not Null</th>
                        <th>Unsigned</th>
                        <th>Auto Incr</th>
                        <th>Zerofill</th>
                    </tr>
                    <tr>
                        <td><input type="text" name="col_name[0]"></td>
                        <td><input type="text" name="datatype[0]"></td>
                        <td><input type="text" name="default[0]"></td>
                        <td><input type="checkbox" name="pk[0]" value=""></td>
                        <td><input type="checkbox" name="null[0]" value=""></td>
                        <td><input type="checkbox" name="unsig[0]" value=""></td>
                        <td><input type="checkbox" name="auto[0]" value=""></td>
                        <td><input type="checkbox" name="zero[0]" value=""></td>
                    </tr>
                </table>
                <input type="submit" value="Create" class="creat">
            </fieldset>
        </div>
        <div class="queries">
            <label for="query"></label>
            <textarea name="query" id="query"></textarea>
            <input type="submit" value="<?= 'Go' ?>">
        </div>
        <div class="query_result">
            <button id="result">Result</button>
            <button id="data">Table Data</button>
            <button id="info">Info</button>
            <div class="result">
            </div>
            <div class="data"></div>
            <div class="info"></div>
            <?php
            require_once("View/PHP/query_result.php");
            ?>
        </div>
    </div>
</div>
<script src="View/JS/create.js"></script>
</body>
</html>