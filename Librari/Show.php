<?php
class SHOW
{
    private $conn;
    private $db;
    private $table="SHOW TABLES FROM  ";
    private $view = "SELECT table_name FROM views WHERE TABLE_SCHEMA = ";
    private $proc = "SELECT `name` FROM proc WHERE db = ";
    private $func = "SELECT `name` FROM proc WHERE `type` = 'FUNCTION' AND db = ";
    private $trig = "SHOW TRIGGERS FROM ";
    private $event = "SELECT event_name FROM EVENTS WHERE EVENT_SCHEMA = ";
    private $tagstart;
    private $tagend;
    public function __construct($conn,$db,$tagstart,$tagend)
    {
        $this->db=$db;
        $this->conn=$conn;
        $this->tagstart=$tagstart;
        $this->tagend=$tagend;
    }

    public function showTables()
    {
        $query="{$this->table}`{$this->db}`";
        $res=$this->conn->query($query);
        if($res->num_rows>0){
            while($row=$res->fetch_assoc()){
                echo $this->tagstart.$row["Tables_in_{$this->db}"].$this->tagend;
            }
        }
        $this->conn->close();
    }

    public function showViews()
    {
        $query="{$this->view}'{$this->db}'";
        $res=$this->conn->query($query);
        if($res->num_rows>0) {
            while ($row = $res->fetch_assoc()) {
                echo $this->tagstart . $row["table_name"] . $this->tagend;
            }
        }
        $this->conn->close();
    }

    public function showProc()
    {
        $query="{$this->proc}'{$this->db}'";
        $res=$this->conn->query($query);
        if($res->num_rows>0) {
            while ($row = $res->fetch_assoc()) {
                echo $this->tagstart . $row["name"] . $this->tagend;
            }
        }
        $this->conn->close();
    }

    public function showFunc()
    {
        $query="{$this->func}'{$this->db}'";
        $res=$this->conn->query($query);
        if($res->num_rows>0) {
            while ($row = $res->fetch_assoc()) {
                echo $this->tagstart . $row["name"] . $this->tagend;
            }
        }
        $this->conn->close();
    }

    public function showTriggers()
    {
        $query=$this->trig.$this->db;

        $res=$this->conn->query($query);
        if($res->num_rows>0) {
            while ($row = $res->fetch_assoc()) {
                echo $this->tagstart . $row["Trigger"] . $this->tagend;
            }
        }
        $this->conn->close();
    }

    public function showEvent()
    {
        $query="{$this->event}'{$this->db}'";
        $res=$this->conn->query($query);
        if($res->num_rows>0) {
            while ($row = $res->fetch_assoc()) {
                echo $this->tagstart . $row["table_name"] . $this->tagend;
            }
        }
        $this->conn->close();
    }

}