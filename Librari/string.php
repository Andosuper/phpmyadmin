<?php
//  select for info
define("FIELD_QUERY","SELECT `COLUMN_NAME` AS `Field`,
CONCAT(`COLUMN_TYPE`,\" \",IF(`IS_NULLABLE` = 'NO', 'NOT NULL', 'NULL')) AS `Type`,
`COLUMN_COMMENT` AS `Comment`
FROM `COLUMNS`
WHERE `TABLE_SCHEMA` = ");
define("ADD","AND `TABLE_NAME` = ");
define("INDEX_QUERY","SHOW INDEXES FROM ");
define("DDL_QUERY","SHOW CREATE TABLE ");
define("TABLE_QUERY", "SELECT `TABLE_NAME` AS `Name` ,
`ENGINE` AS `Engine`,
`TABLE_ROWS` AS `Rows`,
`DATA_LENGTH` AS `Data_size`,
`INDEX_LENGTH` AS `Index_size`
FROM `TABLES`
WHERE `TABLE_SCHEMA` = ");
define("VIEW_QUERY", "SELECT `TABLE_NAME` AS `View_name` ,
`IS_UPDATABLE` AS `Is_updatable`,
`DEFINER` AS `Definer`,
`SECURITY_TYPE` AS `Sequrity_type`
FROM `VIEWS`
WHERE `TABLE_SCHEMA` = ");
define("PROCEDURE_QUERY", "SELECT `name` AS `Name` ,
`definer` AS `Definer`,
`security_type` AS `Security_type`,
`comment` AS `Comment`
FROM `proc`
WHERE `type` = 'PROCEDURE'
AND `db` = ");
define("FUNCTION_QUERY", "SELECT `name` AS `Name` ,
`definer` AS `Definer`,
`security_type` AS `Security_type`,
`comment` AS `Comment`
FROM `proc`
WHERE `type` = 'FUNCTION'
AND `db` = ");
define("TRIGGER_QUERY", "SELECT `TRIGGER_NAME` AS `Trigger` ,
`EVENT_MANIPULATION` AS `Event`,
`EVENT_OBJECT_TABLE` AS `Table`,
`ACTION_TIMING` AS `Timing`,
`SQL_MODE` AS `Sql_mode`,
`DEFINER` AS `Definer`
FROM `TRIGGERS`
WHERE `TRIGGER_SCHEMA` = ");
define("EVENT_QUERY", "SELECT `EVENT_NAME` AS `Name` ,
`DEFINER` AS `Definer`,
`EVENT_TYPE` AS `Type`,
`EXECUTE_AT` AS `Execute_at`,
`INTERVAL_VALUE` AS `Interval_value`,
`INTERVAL_FIELD` AS `Interval_field`,
`STARTS` AS `Starts`,
`ENDS` AS `Ends`,
`STATUS` AS `Status`
FROM `EVENTS`
WHERE `EVENT_SCHEMA` = ");

// field names for info
define("TABLE", "<tr>
<th>Name</th>
<th>Engine</th>
<th>Rows</th>
<th>Data_size</th>
<th>Index_size</th>
</tr>");
define("VIEW", "<tr>
<th>View_name</th>
<th>Is_updatable</th>
<th>Definer</th>
<th>Sequrity_type</th>
</tr>");
define("PROCEDURE", "<tr>
<th>Name</th>
<th>Definer</th>
<th>Security_type</th>
<th>Comment</th>
</tr>");
define("FUNCTIONS", "<tr>
<th>Name</th>
<th>Definer</th>
<th>Security_type</th>
<th>Comment</th>
</tr>");
define("TRIGGER", "<tr>
<th>Trigger</th>
<th>Event</th>
<th>Table</th>
<th>Timing</th>
<th>Sql_mode</th>
<th>Definer</th>
</tr>");
define("EVENT", "<tr>
<th>Name</th>
<th>Definer</th>
<th>Type</th>
<th>Execute_at</th>
<th>Interval_value</th>
<th>Interval_field</th>
<th>Starts</th>
<th>Ends</th>
<th>Status</th>
</tr>");
define("TABLE_FIELD","<tr>
<th>Field</th>
<th>Type</th>
<th>Comment</th>
</tr>");
define("INDEX_FIELD","<tr>
<th>Indexes</th>
<th>Columns</th>
<th>Index type</th>
</tr>");
define("DDL_FIELD","<tr>
<th>Create table</th>
</tr>");