<?php


class INFO
{
    private $conn;
    private $query;
    private $rows;

    /**
     * INFO constructor.
     * @param $conn
     * @param $db
     * @param $query
     */
    public function __construct($conn, $query)
    {
        $this->conn = $conn;
        $this->query = $query;
        $this->changeRows();
    }

    /**
     * @param mixed $db
     */
    public function setConn($conn)
    {
        $this->conn = $conn;
    }

    /**
     * @param mixed $query
     */
    public function setQuery($query)
    {
        $this->query = $query;
        $this->changeRows();
    }

    /**
     * @return mixed
     */
    public function getRows()
    {
        return $this->rows;
    }

    private function changeRows()
    {
        $result = $this->conn->query($this->query);
        $this->rows = $result -> num_rows;
    }

    public function infoDatabase($name)
    {
        $res = $this->conn->query($this->query);
        echo "<table>";
        echo $name;
        if($this->rows>0){
            while ($row = $res->fetch_assoc()) {
                echo "<tr>";
                foreach ($row as $key => $val) {
                    echo "<td>{$row[$key]}</td>";
                }
                echo "</tr>";
            }
        }
        echo "</table>";
    }

    public function infotab()
    {
        
    }
}