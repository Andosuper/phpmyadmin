<?php
$conn = new mysqli("localhost", "root", "");
if ($conn->connect_errno) {
    die(ERROR . $conn->connect_error);
}
$sql = "SHOW DATABASES";
$res = $conn->query($sql);
if (!$res) {
    exit($conn->error);
}
if ($res->num_rows > 0) {

    $i = 0;
    while ($row = $res->fetch_assoc()) {

        $arr[$i] = $row["Database"];
        $i++;
    }
   echo json_encode($arr);
}
