$(document).ready(function () {
    var body=$("body");
    var arr=[];
    $.ajax({
        type: "post",
        datatype: "JSON",
        url: "View/PHP/db_resurs.php",
        success: function (result) {
            arr = JSON.parse(result);
            var out1 = "";
            var out2 = "<li><img class='plus' src='View/images/plus.png'><span id='crdb'>New</span> </li>";
            var src="src=\'View\/images\/folder.png\'>";
            for (i = 0; i < arr.length; i++) {
                out1 += "<option value='"+arr[i]+"'>" +
                    arr[i] + "</option>";
                out2 +="<li>"+
                    "<img class=\'" + arr[i]+"\' "+ src +
                    "<span class='" + arr[i] + "'>" + arr[i] + "</span>" +
                    "<ul class='to" + arr[i] + "\' style=\'display: none\'>" +
                    "<ul class='" + arr[i] + "-ta\'>" +
                    "<img id='" + arr[i] + "-ta\' "+ src +
                    "<span>Tables</span>" +
                    "<li><img class='plus' src='View/images/plus.png'><span id='crtb'>New</span></li>"+
                    "</ul>" +
                    "<ul class=\'" + arr[i] + "-vi\' >" +
                    "<img id=\'" + arr[i] + "-vi\' "+ src +
                    "<span>Views</span>" +
                    "<li><img class='plus' src='View/images/plus.png'><span id='crvi'>New</span></li>"+
                    "</ul>" +
                    "<ul class=\'" + arr[i] + "-pr\'>" +
                    "<img id=\'" + arr[i] + "-pr\' "+ src +
                    "<span>Stored Srocs</span>" +
                    "<li><img class='plus' src='View/images/plus.png'><span id='crpr'>New</span></li>"+
                    "</ul>" +
                    "<ul class=\'" + arr[i] + "-fun\'>" +
                    "<img id=\'" + arr[i] + "-fun\' "+ src +
                    "<span>Functions</span>" +
                    "<li><img class='plus' src='View/images/plus.png'><span id='crfu'>New</span></li>"+
                    "</ul>" +
                    "<ul class=\'" + arr[i] + "-tr\'>" +
                    "<img id=\'" + arr[i] + "-tr\' "+ src +
                    "<span>Triggers</span>" +
                    "<li><img class='plus' src='View/images/plus.png'><span id='crtr'>New</span></li>"+
                    "</ul>" +
                    "<ul class=\'" + arr[i] + "-ev\'>" +
                    "<img id=\'" + arr[i] + "-ev\' "+ src +
                    "<span>Events</span>" +
                    "<li><img class='plus' src='View/images/plus.png'><span id='crev'>New</span></li>"+
                    "</ul>" +
                    "</ul>"+"</li>";
            }
            $(".db").append(out2);
            $("#db").append(out1);
        }
    });
    body.on("click", "img", function () {
        var val = $(this).attr("class");
        if(arr.indexOf(val)!=-1){
            $(".dbname").text(val);
            $(".tbname").css("display", "none");
            $(".fortb").css("display","none");
        }
        var imclas=$('.to' + val);
        var id = $(this).attr("id");
        var type = typeof id;
        var display = imclas.css("display");
        switch (display) {
            case "block":
                imclas.css("display", "none");
                break;
            case "none":
                imclas.css("display", "block");
                break;
        }
        if (type != "undefined") {
            $(".xdebug").remove();
            $.ajax({
                type: "post",
                data: "act=" + id,
                url: "Controller/resurs.php",
                success: function (result) {
                    $("." + id).append(result);
                }
            });
        }
    });
    body.on("click",".tables",function () {
        $(".data").text("");
        var tb_name=$(this).text();
        var to_db =$(this).parent().attr("class");
        var arr = to_db.split("-");
        $(".fortb").css("display","block");
        $(".tbname").css("display","block");
        $(".dbname").text(arr[0]);
        $(".fortb").text("\:");
        $('.tbname').text(tb_name);
        alert(tb_name+" "+arr[0]);
        $.ajax({
            type: "post",
            data: "tb_name="+tb_name+"&to_db="+arr[0],
            url: "Controller/result.php",
            success: function (result) {
                $(".data").append(result);
            }
        });
        $.ajax({
            type: "post",
            data: "tb_name="+tb_name+"&to_db="+arr[0],
            url: "Controller/info.php",
            success: function (result) {                   // INFO
                $(".info").html(result);
            }
        });
    });
    body.on("click","span", function () {
       var name=$(this).attr("class");
        var isdb=arr.indexOf(name);
        if(isdb != -1){
            alert(name);
            $.ajax({
                type: "post",
                data: "useDB="+name,
                url: "Controller/info.php",
                success: function (result) {                   // INFO
                    $(".info").html(result);
                }
            });
        }
    });
    $("#db").mouseout(function () {
        var selval=$(this).val();
        var sval =arr.indexOf(selval);
        if(sval != -1){
            $.ajax({
                type: "post",
                data: "useDB="+selval,
                url: "Controller/query.php"
            });
        }
    });
    $("button").click(function () {
        var data=$(".data");
        var result=$(".result");
        var info=$(".info");
        var act=$(this).attr("id");
        var block=$("."+act);
        switch (act){
            case "result":
                block.css("display", "block");
                data.css("display", "none");
                info.css("display", "none");
                break;
            case "data":
                block.css("display", "block");
                result.css("display", "none");
                info.css("display", "none");
                break;
            case "info":
                block.css("display", "block");
                data.css("display", "none");
                result.css("display", "none");
                break;
        }
    });
    $(".close").click(function () {
        $(".creatTb").css("display","none");
    });
});